#!/usr/bin/env bash

if [[ "$_IMPORT_SOURCED" == "true" ]]; then
    log_debug "import.sh has already been sourced. Skipping."
    exit 0
fi
_IMPORT_SOURCING="true"
# Settings variables
BASH_IMPORT_PATH="${BASH_IMPORT_PATH:-$HOME/.bash_dependencies}"
source "$BASH_IMPORT_PATH/druidgreeneyes/bash-libs/default/logging.sh"
source "$BASH_IMPORT_PATH/druidgreeneyes/bash-libs/default/require.sh"
IMPORT_SUCCESSFUL_IMPORTS=" gitlab:druidgreeneyes/bash-libs@default::logging  gitlab:druidgreeneyes/bash-libs@default::require "

# Functions we will use
_import_parse_git_host () {
    local LOG_CONTEXT=$(add_log_context "_parse_git_host")
    case "$1" in
        "github")
            echo "https://github.com"
            ;;
        "gitlab")
            echo "https://gitlab.com"
            ;;
        "local")
            echo ""
            ;;
        *)
            log_warn "No known git host found matching $1, returning it raw."
            echo "$1"
            ;;
    esac
}

_import_construct_package_path () {
    local package="$1"
    local target_ref="${2:-default}"
    echo "$BASH_IMPORT_PATH/$package/$target_ref"
}

# Primary functions
fetch () {
    set -e
    local LOG_CONTEXT=$(add_log_context "pull")
    if [[ ${#*} -lt 2 ]]; then
        log_error "expecting at least {git_host} and {repo} but called with fewer than 2 arguments!"
        return 1
    fi
    local git_host=$(_import_parse_git_host $1)
    local package="$2"
    local target_ref="$3"

    log_debug "git_host: $git_host" \
              "package: $package" \
              "target_ref: $target_ref"

    require_command git

    local BASH_IMPORT_PATH="${BASH_IMPORT_PATH:-$HOME/.bash_dependencies}"
    local package_path=$(_import_construct_package_path "$package" "$target_ref")

    local git_clone="$git clone $git_host/$package $package_path"

    log_info "cloning $package into $package_path"

    if "$git_clone" >&2; then
        log_info "successfully cloned $package into $package_path"
        if [[ -n "$target_ref" && "$target_ref" != "default" ]]; then
            log_info "checking out target ref $target_ref"
            local _old_dir=$CWD
            cd "$package_path"
            if git checkout "$target_ref" >&2; then
                log_info "successfully checked out $target_ref from $package"
            else
                log_info "unable to checkout $target_ref from $package!" \
                         "double-check to make sure your target is correct, and try again."
                cd $_old_dir
                return 1
            fi
            cd $_old_dir
        fi
    else
        log_error "failed to clone $package into $package_path!"
        return 1
    fi
}

import () {
    set -eu
    local old_import_depth="${import_depth:-0}"
    local import_depth=$(( old_import_depth + 1 ))
    if [[ -z $LOG_CONTEXT || "$LOG_CONTEXT" != *"import($old_import_depth): "* ]]; then
        local LOG_CONTEXT=$(add_log_context "import($old_import_depth)")
    fi
    local LOG_CONTEXT="$(echo "$LOG_CONTEXT" | sed "s/import($old_import_depth):/import($import_depth):/g")"
    local LOG_LEVEL="${LOG_LEVEL:-info}"

    if [[ -z $IMPORT_SUCCESSFUL_IMPORTS ]]; then
        IMPORT_SUCCESSFUL_IMPORTS=""
    fi

    local git_host="$1"
    require_var git_host "called with no git host specified!"

    local package_string="$2"
    require_var package_string "called with no package specified!"

    local libs="${*:3}"

    log_debug "git_host: $git_host" \
              "package_string: $package_string" \
              "libs: $libs"

    local cache_ref="$git_host:$package_string"
    touch "$BASH_IMPORT_PATH/CACHE"

    if [[ "$package_string" != *"@"* ]]; then
        local package_string="$package_string@default"
    fi

    local target_ref="${package_string#*@}"
    local package="${package_string%@*}"

    if [[ -n "$target_ref" ]]; then
        log_debug "target_ref: $target_ref"
    fi

    local package_path=$(_import_construct_package_path "$package" "$target_ref")

    if [[ -d "$package_path" ]]; then
        log_info "$cache_ref already exists locally." \
                 "we will not try to fetch it again."
    else
        log_info "attempting to fetch $cache_ref"
        fetch "$git_host" "$package" "$target_ref"
        local fail=$?
        if [[ $fail -gt 0 ]]; then
            log_error "failed to fetch $cache_ref!"
            return "$fail"
        fi
    fi

    local lib_config="$package_path/.lib_config"
    local lib_source_path=""

    if [[ -f "$lib_config" ]]; then
        log_info "Reading lib config from $lib_config"
        for pair in "$(grep -v '#' "$lib_config")"; do
            local var="${pair#*::}"
            local val="${pair%::*}"
            log_debug "setting $var to $val (from $lib_config)"
            eval "local lib_$var=$val"
        done
    else
        log_warn "Package $package contains no lib config at $lib_config!"
    fi

    if [[ -z "$lib_source_path" ]]; then
        log_warn "Library $git_host:$package_string specifies no source path in its .lib_config." \
                 "We will assume all libraries are at the project root."
    else
        log_info "Library $git_host:$package_string specifies source path $lib_source_path"
    fi

    local lib_path="$package_path/$lib_source_path"
    local lib_path="${lib_path%/}"

    if [[ -z "$libs" ]]; then
        log_warn "No libs explicitly called for." \
                 "We encourage requesting specific libs from any given package, to reduce the chance of functions or variables clobbering eachtoher in the final environment." \
                 "Importing all libs from $package..."
        local libs="$(find "$lib_path" -name "*.lib.sh" -depth 1 \
           | sed -E -e 's_.*/__g' -e 's/.lib.sh//g')"
        log_info "Found the following libs: $libs"
    fi

    for lib in $libs; do
        local lib_id="$git_host:$package@$target_ref::$lib"
        if [[ "$IMPORT_SUCCESSFUL_IMPORTS" == *" $lib_id "* ]]; then
            log_info "$lib_id has already been imported!"
        else
            local lib_file="$lib_path/$lib.sh"
            if [[ ! -f "$lib_file" ]]; then
                log_error "expect lib file $lib_file does not exist!"
                log_error "libs exposed by package $package:"
                echo
                ls $lib_path | grep ".sh" | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1
                echo
                return 1
            fi
            log_info "attempting to import $lib_id at $lib_file..."
            source "$lib_file"
            IMPORT_SUCCESSFUL_IMPORTS="$IMPORT_SUCCESSFUL_IMPORTS $lib_id "
            log_info "successfully imported $lib_id from $lib_file!"
        fi
    done
}

unset _IMPORT_SOURCING
_IMPORT_SOURCED="true"
